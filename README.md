# Sample Rust guessing game
Rust tutorial project from "THE BOOK"
see here: https://doc.rust-lang.org/book/ch02-00-guessing-game-tutorial.html
## Running the game

Use `cargo` to build and start the game:

```bash
$ cargo run
```